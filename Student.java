public class Student{
	public String name;
	public int age;
	public String program;
	
	public void intro(){
		if(name.equals("Alex")){
			System.out.println("Hi, I'm " + name + " and I'm in " + program);
		}else{
			System.out.println("Hi, I'm " + name + " and I'm in " + program);
		}
	}
	
	public void canParticipateEvent(){
		if(age >= 18){
			System.out.println("You are old enough, so you can participate in this event");
		}else{
			System.out.println("You are not old enough to participate in this event");
		}
	}
}