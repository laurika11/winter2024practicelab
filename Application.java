public class Application{
	public static void main(String[] args){
		Student student1 = new Student();
		student1.name = "Alex";
		student1.age = 17;
		student1.program = "Social Science";
		
		Student student2 = new Student();
		student2.name = "Sara";
		student2.age = 18;
		student2.program = "Health Science";
		
		System.out.println(student1.name + " " + student1.age + " " + student1.program);
		System.out.println(student2.name + " " + student2.age + " " + student2.program);
		
		student1.intro();
		student2.intro();
		
		student1.canParticipateEvent();
		student2.canParticipateEvent();
		
		Student[] section4 = new Student[3];
		section4[0] = student1;
		section4[1] = student2;
		section4[2] = new Student();
		section4[2].name = "Max";
		section4[2].age = 19;
		section4[2].program = "Computer Science";
		
		System.out.println(section4[2].name + " " + section4[2].age + " " + section4[2].program);
		
	}
}